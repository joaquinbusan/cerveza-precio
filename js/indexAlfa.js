
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })

    var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
    var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
            return new bootstrap.Popover(popoverTriggerEl)
        })
    
    var myCarousel = document.querySelector('#carouselExampleDark')
    var carousel = new bootstrap.Carousel(myCarousel)
    
    $(function () {
        $('#exampleModal').on('show.bs.modal', function (e) {
            console.log('Modal contacto se inicia el proceso para mostrar');

            $('#botonModal').removeClass('btn-dark');
            $('#botonModal').addClass('btn-primary');
            $('#botonModal').prop('disabled', true);
        });
        $('#exampleModal').on('shown.bs.modal', function (e) {
            console.log('Modal contacto ya se esta mostrando');
        });
        $('#exampleModal').on('hide.bs.modal', function (e) {
            console.log('Modal contacto se inicia el proceso para ocultar');
        });
        $('#exampleModal').on('hidden.bs.modal', function (e) {
            console.log('Modal contacto ya esta oculto');
            $('#botonModal').prop('disabled', false);
        });
    });
